export const user = {
    logined : false,
    user : null,
    auth : [],
    menu : [],
}

export const setLogined = (u) =>{
    user.logined = true
    user.user = u
}

export const setLoginout = () =>{
    user.user = null
    user.logined = false
    window.location.href="/"
}

export const isLogined = () =>{
    return user.logined
}