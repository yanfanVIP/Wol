
import http from './http'
import { setLogined } from '../data/USER'

export const login = async(form) =>{
    let user = await http.fetchPost(`/api/login/`, form)
    if(user){
        setLogined(user)
    }
    return user
}

export const loadList = async() =>{
    return await http.fetchGet(`/api/wol/list`)
}

export const add = async(form) => {
    return await http.fetchPost(`/api/wol/add`, form)
}

export const del = async(name) => {
    return await http.fetchPost(`/api/wol/del/${name}`)
}

export const wake = async(name) => {
    return await http.fetchPost(`/api/wol/wake/${name}`)
}