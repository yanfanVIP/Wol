import { HashRouter, Switch, Route, Redirect } from 'react-router-dom';
import React from 'react';
import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import Add from './pages/Add';

const router = () => {
  return (
    <HashRouter>
      <Switch>
        <Route key={'login'} path={'/login'} component={Login}/>
        <Route key={'dashboard'} path={'/dashboard'} component={Dashboard}/>
        <Route key={'add'} path={'/add'} component={Add}/>
        <Redirect exact from="/" to="/login" />
      </Switch>
    </HashRouter>
  );
};

export default router();
