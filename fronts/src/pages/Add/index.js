import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import IceContainer from '@icedesign/container';
import { Input, Button, Message } from '@alifd/next';
import {
  FormBinderWrapper as IceFormBinderWrapper,
  FormBinder as IceFormBinder,
  FormError as IceFormError,
} from '@icedesign/form-binder'
import PageHead from '../PageHead'
import * as Service from '../../service'

@withRouter
export default class AddForm extends Component {

  constructor(props) {
    super(props);
    if(props.match.params && props.match.params.id){
      this.state = {
        id : props.match.params.id,
        loading : true,
        value : {}
      }
    }else{
      this.state = {
        loading : false,
        value: {},
      }
    }
  }

  componentDidMount = async() => {
    if(this.state.id){
      let data = await Sc_merchant.detail(this.state.id)
      let image = [{
        name : data.image,
        imgURL : data.image
      }]
      let state = {
        context : BraftEditor.createEditorState(data.context),
        value: { 
          ...data, 
          image : image,
        },
        loading : false
      };
      this.setState(state)
    }
  }

  validateAllFormField = () => {
    this.refs.form.validateAll(async(errors, values) => {
      if (errors) { return; }
      await Service.add(values)
      Message.success('提交成功');
      this.props.history.push({ pathname : '/dashboard' });
    });
  };

  render() {
    return (
      <div>
        <PageHead title="添加" />
        <IceContainer style={{ padding: '40px' }} loading={this.state.loading}>
          <IceFormBinderWrapper value={this.state.value} ref="form" >
            <div style={styles.formItem}>
              <div style={styles.formLabel}>名称：</div>
              <IceFormBinder name="name" required message="必填">
                <Input placeholder="请输入名称"/>
              </IceFormBinder>
              <div style={styles.formError}>
                <IceFormError name="name" />
              </div>
            </div>
            <div style={styles.formItem}>
              <div style={styles.formLabel}>MAC: </div>
              <IceFormBinder name="mac" required message="必填">
                <Input placeholder="请输入MAC"/>
              </IceFormBinder>
              <div style={styles.formError}>
                <IceFormError name="mac" />
              </div>
            </div>
            <div style={styles.formItem}>
              <div style={styles.formLabel}>IP：</div>
              <IceFormBinder name="ip" required message="必填">
                <Input placeholder="请输入IP"/>
              </IceFormBinder>
              <div style={styles.formError}>
                <IceFormError name="ip" />
              </div>
            </div>
            <Button type="primary" onClick={this.validateAllFormField}>
              提 交
            </Button>
          </IceFormBinderWrapper>
        </IceContainer>
      </div>
    );
  }
}

const styles = {
  formItem: {
    marginBottom: '30px',
    display: 'flex',
    alignItems: 'center',
  },
  formLabel: {
    fontWeight: '450',
    width: '80px'
  },
  formError: {
    marginTop: '10px',
  },
  button: {
    marginLeft: '100px',
  },
  editorContent: {
    minHeight: '600px',
  },
};
