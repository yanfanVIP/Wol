package com.geekcode.wol.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer{
	
	@Autowired
	LoginHandlerInterceptor loginHandle;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(loginHandle)
			.addPathPatterns("/api/**")
			.excludePathPatterns("/api/login/");
		WebMvcConfigurer.super.addInterceptors(registry);
	}
}
