package com.geekcode.wol.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.geekcode.wol.model.User;
import com.geekcode.wol.util.SessionUtil;

@RestController
@RequestMapping("/api/login")
public class LoginAPI extends BaseAPI{

	@Value("${system.username}")
	String username;
	
	@Value("${system.password}")
	String password;
	
	@PostMapping("/")
	public Object arp(HttpServletRequest request, @RequestBody User user) {
		if(username.equals(user.getUsername()) && password.equals(user.getPassword())){
			SessionUtil.setSession(request, "USER", user);
			return SUCCESS(user);
		}
		return FAIL(401, "登录失败");
	}
}
