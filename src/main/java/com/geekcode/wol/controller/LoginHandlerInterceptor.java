package com.geekcode.wol.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import com.geekcode.wol.model.User;
import com.geekcode.wol.util.SessionUtil;

@Component
public class LoginHandlerInterceptor implements HandlerInterceptor {
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		User user = SessionUtil.getSession(request, "USER");
		if(user != null){
			return HandlerInterceptor.super.preHandle(request, response, handler);
		}
		response.sendError(401, "need login");
    	return false; 
	}
}
