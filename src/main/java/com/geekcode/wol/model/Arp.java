package com.geekcode.wol.model;

import java.io.Serializable;

public class Arp implements Serializable{
	private static final long serialVersionUID = 9133267587267819387L;
	
	String name;
	String mac;
	String ip;
	Integer status;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
}
